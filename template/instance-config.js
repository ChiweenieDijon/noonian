// see https://gitlab.com/ChiweenieDijon/noonian/wikis/Instance-Configuration

module.exports = {
  
  instanceName:'#INSTANCE_NAME#', //Used for referring to the instance via the CLI

  instanceId:'#INSTANCE_ID#', //Used in versioning the persisted business objects (keep it short)
  

  serverListen: {
    port: #LISTEN_PORT#,
    host: '#LISTEN_ADDRESS#'
  },

  // MongoDB connection options
  mongo: {
    uri: '#DATABASE_URI#'
  },

  // Secret for session; used for signing the auth token
  secrets: {
    session: '#SESSION_SECRET#'
  },

  // Set the level for instance.log: error,warn,info,verbose,debug,silly
  logLevel:'debug',


  /*
    Use urlBase if noonian will be served to a path other than the root of the domain
    (e.g. one domain can host multiple noonian instances as such: mydomain.com/instance1, mydomain.com/instance2)
  */
  //urlBase:'subdir',

  /*
    Maintain history of changes to Business Objects;
    retains diffs in UpdateLog's as updates are made
  */
  //enableHistory:true,


/*
   If you are using this instance to build a package, enable it here
*/
  // packageBuilding:{
  //   key:'PACKAGE_KEY',
  //   fsSync:true
  // },

/*
  Configure two-factor authentication
*/
  // twoFactorAuth:{
  //   "userToPhone":"user.phone",
  //   "implementation": "Twilio",
  //   "condition": "ROLE_SYSADMIN",
  //   "refresh_period": 24
  // },


/*
  Configure "login preconditions"
    redirect a user to a particular page if their user record matches certain condition
*/
  // loginPreconditions:{
  //   condition:'!user.agreed_terms', //condition evaluated against user object 
  //   redirect:'/usage_terms.html',   //redirect path for when condition not met
  //   allow:'/ws/agreeToTerms|/img/logo.jpg' //regex describing paths used for resolving the condition
  // },


/*
  Can set up express to use https w/ key/cert.
  However, it is recommended instad to listen on localhost and proxy through apahce or NGINX
*/
  // useHttps:false,

  // ssl: {
  //   keyFile: 'server.key',
  //   certFile: 'server.crt'
  // },


/*
  Pass arbitrary settings to the express application
  key/values are passed to express().set(...)
*/
  // expressSettings: {
  //   //use this if you're proxying behind apache, but want the req.ip to reflect the originating ip address:
  //   'trust proxy':'loopback' 
  // },


/*
  specify directories from which to serve static files from filesystem (using express.static())
  - checked BEFORE any noonian WebService or WebResource is retreived. 
  - can be an array to serve from multiple directories; non-absolute paths are resolved relative to instance directory
  - NOTE: if instance directory contains "client", it is used without any special configuration here.
*/
  // staticContent: '/some/other/directory/client',


/*
  maximum size of body that can be POSTed to web server (body-parser "limit" parameter)
*/
  // maxRequestBodySize:'10mb'


};
