Noonian -- a full-stack Javascript platform for database and web application development.
==============================

## Intro
[![Audi R8](http://img.youtube.com/vi/oVNOhNQyxQ0/0.jpg)](https://www.youtube.com/watch?v=oVNOhNQyxQ0 "Noonian Intro/Tutorial")

## Installation 

### Install Prerequisites

Before proceeding, ensure the following software packages are installed on the machine:

1. [Node.js](https://nodejs.org/en/download/)
2. [MongoDB](https://www.mongodb.com/download-center?jmp=nav#community)
3. [bower](https://bower.io)


### Pull from npm

```bash
npm install -g noonian
```


*NOTE: if you are installing to a system-wide node install (as opposed to a node installation that is owned by a non-root user), 
there may be issues installing dependencies compiled via node-gyp.  To get around the issue, use the "unsafe-perm" parameter:*

```bash
npm install --unsafe-perm -g noonian
```

*This will be resovled in a future version that will have refactored out any dependencies that are not pure javascript.*

## Instance set-up

[See the "getting started" guide on readthedocs](https://noonian.readthedocs.io/en/latest/development-guide/getting-started.html)

## License

GNU AFFERO GENERAL PUBLIC LICENSE, Version 3

Copyright (C) 2016-2020  Eugene Newall-Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
