#should be invoked via "npm run dockerbuild"
VERSION=`npm view . version`
npm pack
mv noonian*.tgz build/
cd build
docker build -t chiweeniedijon/noonian:$VERSION .
rm noonian*.tgz
