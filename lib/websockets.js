/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** 
  Web sockets "switchboard"
  @param server object returned from http.createServer(...)
*/

const _ = require('lodash');

const url = require('url');
const WebSocket = require('ws');

const db = require('./api/datasource');
const datatrigger = db._svc.DataTriggerService;
const auth = require('./api/auth');
const invokerTool = require('./util/invoker.js');

const logger = require('./api/logger').get('sys.websockets');


var websocketBosByPath;

var onWebSocketConnection = function(ws, req) {
    
    const location = url.parse(req.url, true);
    
    logger.debug('WebSocket: requested location: %j', location);
    
    var path = location.pathname;
    var queryParams = location.query;
    
    var wsObj = websocketBosByPath[path];
    if(!wsObj) {
        ws.terminate();
        ws.emit('error', new Error('Bad location requested'));
        return;
    }
    
    logger.debug('matched WebSocket %s', wsObj._id);
    
    //Check permissions...
    auth.checkRoles(req, wsObj.rolespec).then(
      function() {
          var injectables = {
              ws:ws,
              req:req,
              queryParams:queryParams
          };
          
          var toCall = wsObj['on_connect'];
          logger.debug('invoking %s', toCall);
          
          invokerTool.invokeAndReturnPromise(toCall, injectables, wsObj).then(
            function(retVal) {
                //TODO Do anything with return value?
                logger.debug('on_connect returned %j', retVal);
            },
            function(err) {
              logger.error('error invoking WebSocket on_connect for $s', path).error(err);
              ws.terminate();
              ws.emit('error', err);
              
            }
          );//end invokerTool.invoke

      },
      function(err) {
          ws.terminate();
          ws.emit('error', err);
      }
    );//end auth.checkRoles


}

module.exports = function(server) {
    
    if(!db.WebSocketServer) {
        logger.error('Missing WebSocket Business Object class; upgrade system to use websockets');
        return;
    }
    
    db.WebSocketServer.find({}).then(function(wsBos) {
        websocketBosByPath = _.indexBy(wsBos, 'path');
    });
    
    //Watch WebSocket objects
    datatrigger.registerDataTrigger('sys.internal.websocket', db.WebSocketServer._bo_meta_data.bod_id, 'after', true, true, true, function(isCreate, isUpdate, isDelete) {
        if(isDelete) {
            delete websocketBosByPath[this._previous.path];
        }
        else if(isCreate || isUpdate) {
            websocketBosByPath[this.path] = this;
        }
        
        if(isUpdate && this._previous.path !== this.path) {
            delete websocketBosByPath[this._previous.path];
        }
        
        return null;
    });
        
    //Set up the server
    var wss = new WebSocket.Server({server});
    wss.on('connection', onWebSocketConnection);
    wss.on('error', function(err) {
        logger.error('WEBSOCKET SERVER ERROR').error(err);
    });
}
