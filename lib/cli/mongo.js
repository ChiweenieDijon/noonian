const fs = require('fs');
const path = require('path');
const {spawnSync} = require('child_process');

const tar = require('tar');
const rimraf = require('rimraf');
const moment = require('moment');


const common = require('./common');

const getInstanceDir = common.getInstanceDir;
const getInstanceConf = common.getInstanceConf;


const getMongoDbName = function(conf, instanceDir) {
	if(!conf || !conf.mongo || !conf.mongo.uri) {
		console.error('Couldnt find a valid instance config in %s'.red, instanceDir);
		process.exit(1);
	}

	const dbRegex = /mongodb:\/\/[^\/]+\/([^\/]+)/;

	let parsed = dbRegex.exec(conf.mongo.uri);
	const dbName = parsed && parsed[1]
	
	if(!dbName) {
		console.error('Problem determining DB name from "%s"'.red, conf.mongo.uri);
		process.exit(1);
	}
	return dbName;
};


exports.dumpDb = function(name, cmd) {
	const instanceDir = getInstanceDir(name, cmd);
	const conf = getInstanceConf(instanceDir);
	const dbName = getMongoDbName(conf, instanceDir);

	const dumpDir = path.resolve(instanceDir, 'mongodump');
	try {
		fs.statSync(dumpDir);
	}
	catch(err) {
		if(err.code !== 'ENOENT') {
			console.log(err);
		}
		fs.mkdirSync(dumpDir);
	}

	try {
		const spawnOpts = {
			cwd:dumpDir,
			stdio: 'inherit'
		};

        const callResult = spawnSync('mongodump', ['--db', dbName], spawnOpts);
        if(callResult.status !== 0) {
        	console.error('mongodump returned error status: %s'.red, callResult.status);
        	process.exit(1);
        }
        
    }
    catch(err) {
    	console.error('Problem spawning mongodump executable'.red);
    	console.error(err);
		process.exit(1);        
    }

    const ds = moment().format('YYYY-MM-DD_hhmmss');
    const properDumpDir = path.resolve(dumpDir, ds);


    //Move the dump directory 
    fs.renameSync(
    	path.resolve(dumpDir, 'dump', dbName),
    	properDumpDir
    );
    fs.rmdirSync(path.resolve(dumpDir, 'dump'));


    if(!cmd.zip) {
    	console.log('Succesfully dumped %s to %s', dbName, properDumpDir);
    }
    else {
    	const dumpFile = path.resolve(dumpDir, ds+'.tgz');
    	tar.c(
    		{
    			gzip:true,
    			cwd:dumpDir,
    			file:dumpFile
    		},
    		[ds]
    	)
    	.then(result=>{
			rimraf.sync(properDumpDir);

			console.log('Successfully dumped %s to archive %s', dbName, dumpFile);
			process.exit(0);
		})
		.catch(err=>{
    		console.error('problem zipping dump dir'.red);
			console.error(err);
			process.exit(1);
    	})
    	;

    }


};


exports.dbShell = function(name, cmd) {

	const instanceDir = getInstanceDir(name, cmd);
	const conf = getInstanceConf(instanceDir);
	const dbName = getMongoDbName(conf, instanceDir);

	try {
		const spawnOpts = {
			stdio: 'inherit'
		};

        const callResult = spawnSync('mongo', [dbName], spawnOpts);
        if(callResult.status !== 0) {
        	console.error('mongo returned error status: %s'.red, callResult.status);
        	process.exit(1);
        }
        
    }
    catch(err) {
    	console.error('Problem spawning mongo executable'.red);
    	console.error(err);
		process.exit(1);        
    }

};