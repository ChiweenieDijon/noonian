/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const Q = require('q');
const _ = require('lodash');

const db = require('./index');
const mongoose = require('mongoose');

const Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;

//TODO refactor away from gridfs-stream: https://www.npmjs.com/package/mongoose-gridfs
//Updated mongo api supports streaming: https://mongodb.github.io/node-mongodb-native/3.1/tutorials/gridfs/streaming/
// ancient bug report:  https://github.com/aheckmann/gridfs-stream/issues/125
eval(`Grid.prototype.findOne = ${Grid.prototype.findOne.toString().replace('nextObject', 'next')}`);

const logger = require('../logger').get('sys.db.gridfs');

var gfs;

var filesCollection;

exports.init = function(conf) {
  logger.info('initializing GridFs service');
  var deferred = Q.defer();
  // // var conn = mongoose.createConnection(conf.mongo.uri, conf.mongo.options);
  // const conn = mongoose.connection;
  const conn = db.mongooseConnections.system || db.mongooseConnections.default;

  // conn.once('open', function () {
    logger.verbose('...configuring gridfs-stream');
    gfs = Grid(conn.db);
    filesCollection = conn.db.collection('fs.files');
    deferred.resolve(true);
  // });

  return deferred.promise;
};


/**
 * Saves a file, returning it's id
 **/
exports.saveFile = function(readStream, metadata) {
  var deferred = Q.defer();

  var id = db.generateId();

  var opts = {_id:id};
  if(metadata) {
    opts.metadata = metadata;

  }

  logger.debug('gfs.write %j', opts);

  var ws = gfs.createWriteStream(opts);

  ws.on('finish', function() {
    logger.verbose('finished saving %s', id);
    deferred.resolve(id);
  });
  ws.on('error', function(err) {
    logger.debug('error saving %s', id).debug(err);
    deferred.reject(err);
  });

  readStream.pipe(ws);
  return deferred.promise;
};

/**
 * Opens a write stream to file and returns it;
 * updates metadata object to include attachment_id
 **/
exports.writeFile = function(metadata) {
  var id = db.generateId();

  var opts = {_id:id};
  if(metadata) {
    opts.metadata = metadata;
    metadata.attachment_id = id;
  }

  logger.debug('gfs.writeFile %j', opts);

  return gfs.createWriteStream(opts);
}

/**
 * Retreives a file by id
 * @return { readstream:ReadableStream, metadata:{...} }
 **/
const getFile =
exports.getFile = function(fileId) {
  var deferred = Q.defer();
  logger.debug('gfs.getFile %s', fileId);
  gfs.findOne({filename:fileId}, function (err, file) {
    
    if(err || !file) {
      logger.debug('failed getFile %j', file).debug(err);

      return deferred.reject(err || 'file not found in data store');
    }

    deferred.resolve({
      readstream:gfs.createReadStream({filename:fileId}),
      metadata:file.metadata
    });
  });

  return deferred.promise;
};


const deleteFile =
exports.deleteFile = function(fileId) {
  var deferred = Q.defer();
  logger.debug('gfs.deleteFile %s', fileId);

    gfs.remove({filename:fileId}, function (err) {
      
      if(err) {
        logger.debug('failed deleteFile %j', fileId).debug(err);

        return deferred.reject(err);
      }

      deferred.resolve({
        result:'success',
        file:fileId
      });
    });

    return deferred.promise;
}

/**
 * Add metadata to track incoming reference to a file
 **/
exports.annotateIncomingRef = function(fileId, boClass, boId, field) {
  gfs.findOne({ filename:fileId}, function (err, file) {
    if(err) {
      logger.error("Error saving incoming ref on attachment %s", fileId);
      return;
    }

    if(!file || !file.metadata) {
      logger.error('problem updating incoming refs for file %s from $s', fileId, boClass);
      return;
    }

    var refs = file.metadata.incomingRefs;
    if(!refs) {
      refs = [];
    }

    var found=false;
    _.forEach(refs, function(refDesc) {
      if(refDesc.boClass == boClass && refDesc.boId == boId && refDesc.field === field)
        found = true;
    });

    if(!found) {
      refs.push({boClass:boClass, boId:boId, field:field});

      filesCollection.updateOne({ filename:fileId },
        { $set: { 'metadata.incomingRefs' : refs } },
        function(err, result) {
          if(err)  {
            logger.error('ERROR updating file metadata %s %s', fileId, err);
          }
          else {
            logger.verbose("Updated the file %s metadata %j", fileId, refs);
          }
        }
      );
    }

  });
}



/**
  Get all files in fs.files; return object: 
  keys: filename/attachment_id
  values: metadata for file
*/
const getAllFileMetadata = 
exports.getAllFileMetadata = function() {
    const deferred = Q.defer();
    filesCollection.find({}, {filename:1, metadata:1}, (err, fileList)=>{
        if(err) return deferred.reject(err);
        
        var fmap = {};
        
        fileList.forEach(f=>{
            fmap[f.filename] = f.metadata;
        });
        
        deferred.resolve(fmap);
    });
    
    return deferred.promise;
};

/**
* Export an attachment from gridfs to the filesystem
**/
exports.exportFile = function(attachmentId, outPath) {
return getFile(attachmentId).then(gfsObj=>{
      const deferred = Q.defer();
      
      const inStream = gfsObj.readstream;
      const outStream = fs.createWriteStream(outFile, 'binary');
      
      inStream.pipe(outStream);
      outStream.on('finish', ()=>{
          deferred.resolve(outFile);
      });
      outStream.on('error', err=>{
          deferred.reject(err);
      })
      
      return deferred.promise;
  });
};

/**
* Import a file from the filesystem, return attachment meta object
*  (which can be assigned to a field of type "attachment")
**/
exports.importFile = function(inPath, contentType) {
    const mime = require('mime');

    const deferred = Q.defer();
        
    fs.stat(inFile, (err, stats)=>{
        if(err) {
            return deferred.reject(err);
        }
        const filename = path.basename(inFile);
        const metaObj = {
            filename,
            type:(contentType || mime.lookup(path.extname(filename))),
            size:stats.size
        };
        
        const inStream = fs.createReadStream(inPath, 'binary');
        const savePromise = GridFsService.saveFile(fileStream, metaObj).then(attId=>{
            metaObj.attachment_id = attId;
            return metaObj;
        });
        
        deferred.resolve(savePromise);
    });
    
    
    return deferred.promise;
};


/**
* AttachmentScanner constructor for utility to scan for consistency between Business Objects
* and attachments in gridfs
**/
const AttachmentScanner = 
exports.AttachmentScanner = function() {
  this.existingRefs = {};
  this.missingFiles = {};
};



AttachmentScanner.prototype.getAttachmentFields = function() {
  return db.BusinessObjectDef.find({}, {class_name:1, definition:1}).then(bodList=>{
    const attFields = {};

      _.forEach(bodList, bod=>{
          const className = bod.class_name;
          _.forEach(bod.definition, (td, field)=>{
              if(td && td.type === 'attachment') {
                  attFields[className] = attFields[className] || [];
                  attFields[className].push(field);
              }
          });
      });

      return attFields;
    });

}


AttachmentScanner.prototype.checkObjects = function(className, fieldList) {
    const qry = {}, proj = {};
    fieldList.forEach(f=>{
        proj[f] = 1;
        qry[f] = {$exists:true};
    });
    
    return db[className].find(qry, proj).then(objList=>{
        _.forEach(objList, obj=>{
            fieldList.forEach(f=>{
                let attId = obj[f] && obj[f].attachment_id;
                
                if(attId) {
                    let myRef ={
                        className, field:f, id:obj._id
                    };
                    
                    let refArr = this.existingRefs[attId] = this.existingRefs[attId] || [];
                    refArr.push(myRef);
                    
                    if(!this.existingFiles[attId]) {
                        //Missing from fs.files!
                        myRef.missing = true;
                        this.missingFiles[attId] = true;
                    }
                }
            });//end fieldList iteration
        });//end objList iteration
    });
};

AttachmentScanner.prototype.scan = function() {
  return getAllFileMetadata().then(fileMap=>{
        this.existingFiles = fileMap;
        
        //all attachment_id's are now keys in this.existingFiles;
        
        return this.getAttachmentFields();
    })
    .then(attFields=>{
      let promiseChain = Q(true);
      
      _.forEach(attFields, (fieldList, className)=>{
          promiseChain = promiseChain.then(this.checkObjects.bind(this, className, fieldList));
      });
      
      return promiseChain;
        
    })
    .then(()=>{
        //Check for orphans
        var orphanFiles = this.existingFiles;
        
        Object.keys(this.existingRefs).forEach(filename=>{
            delete orphanFiles[filename];
        })
        
        this.orphanFileIds = Object.keys(orphanFiles);
        
        return {
            summary:{
                missing:Object.keys(this.missingFiles).length,
                orphan:this.orphanFileIds.length
            }, 
            missingFiles:this.missingFiles, 
            orphanFiles, 
            existingRefs:this.existingRefs
        };
    })
};



exports.cleanup = function() {
  return new AttachmentScanner().scan().then(scanResult=>{
    let promiseChain = Q(true);
    var deleteCount = 0;

    _.forEach(scanResult.orphanFiles, (md, fileId)=>{
      deleteCount++;
      promiseChain = promiseChain.then(deleteFile.bind(null, fileId));
    })

    return promiseChain.then(()=>{
      return {
        deleteCount,
        deleted:scanResult.orphanFiles
      }
    });
  });
}