/*
Copyright (C) 2016-2020  Eugene Newall-Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
/**
  datasource
  The server-side api for retrieving data models from mongo.
*/
const fs = require('fs');
const path = require('path');
const Q = require('q');
const _ = require('lodash');

const METADATA = Symbol.for('metadata');

const serverConf = require('../../conf');

const logger = require('../logger').get('sys.db');

const util = require('util');

const mongoose = require('mongoose');
require('mongoose-function')(mongoose);

const uuid = require('uuid/v4');

const Schema = mongoose.Schema,
  ObjectId = Schema.Types.ObjectId,
  Mixed = Schema.Types.Mixed;

const interceptor = require('./mongoose_intercept');

exports._svc = {};
const FieldTypeService = exports._svc.FieldTypeService = require('./fieldtypes');
const DataTriggerService = exports._svc.DataTriggerService = require('./datatrigger');
const QueryOpService = exports._svc.QueryOpService = require('./query');
const GridFsService = exports._svc.GridFsService = require('./gridfs');
const RefService = exports._svc.RefService = require('./references');
const PackagingService = exports._svc.PackagingService = require('./packaging');

const invokerTool = require('../../util/invoker');



//Special minimal "bootstrap" schema/model
const BusinessObjectDefBootstrapSchema = new Schema(
   {
    _id:{ type:String },//_id:{ type:String, index:{unique: true} },
    class_name:  String,
    superclass: Mixed,
    abstract: Boolean,
    definition: Mixed,
    system:Boolean
  },
  {collection:'BusinessObjectDef', strict:false} //strict:false -> any fields absent from this schema will be included on db write
);

var BusinessObjectDefBootstrapModel;

//keyed by "system", "application", or any custom key 
const mongooseConnections = exports.mongooseConnections = {};

const modelCache = {}; //Cache for compiled mongoose models
const modelById = {};//auxilary mapping for model cache: maps BusinessObjectDef id to cached model

const customSchemaConstructors = {};//maps BusinessObjectDef id to the mongoose schema

const getBootstrapModel = function() {
  const mongooseConn = mongooseConnections.system || mongooseConnections.default;

  if(!mongooseConn) {
    throw new Error('Missing system or default mongo config');
  }

  BusinessObjectDefBootstrapModel = mongooseConn.model('BusinessObjectDef_bootstrap', BusinessObjectDefBootstrapSchema);
  return BusinessObjectDefBootstrapModel;

};

/**
 * Generates a random UUID and returns in URL-safe base64.
 **/
const generateId =
exports.generateId = function() {
  var buffer = Buffer.alloc(16);

  uuid(null, buffer, 0);
  return buffer.toString('base64').substr(0, 22).replace(/\//g,'_').replace(/\+/g,'-');
}


/**
 * For Mongoose custom creation of _id on save().
 * passed to the schema.pre('save')
**/
var idGenerator = function(next) {
  if(!this.isNew || this._id) return next();

  this._id = generateId();

  return next();
}

/**
 *  tacked on as a virtual field _disp to all instances
**/
var dispGenerator = function() {
    var typeDescriptor = this._bo_meta_data.type_desc_map;
    var dispTemplate = typeDescriptor._disp;
    try {
      if(dispTemplate) {
        var templateString, options;
        if(typeof dispTemplate === 'string') {
          templateString = dispTemplate;
        }
        else {
          templateString = dispTemplate.template;
          options = dispTemplate.options || {variable:'bo'};
        }
        return _.template(templateString, options)(this);
      }
      else if(typeDescriptor.name && this.name) {
        return this.name;
      }
      else if(typeDescriptor.title && this.title) {
        return this.title;
      }
      else if(typeDescriptor.key && this.key) {
        return this.key;
      }
    }
    catch(err) {} //Might happen if error in dispTemplate...

    return this._bo_meta_data.class_name+'['+this._id+']';

}

var matchTextGenerator = function() {    
    var typeDescriptor = this._bo_meta_data.type_desc_map;
    var templateArr = typeDescriptor._fulltext_matches;
    
    if(templateArr) {      
      var result = [];
      var THIS = this;
      _.forEach(templateArr, function(t) { 
        try {
          result.push(_.template(t)(THIS));
        }
        catch(err) {} //Might happen if error in a template...
      });
      return result;
    }
    
    return undefined;
}

/**
 * Method on _bo_meta_data that retrieves the typeDescriptor object for the specified path.
 * path can be  a simple fieldname or dotted into reference fields, e.g.:
 *   db.SomeBusinessObj._bo_meta_data.getTypeDescriptor('refField.blah');
 **/
const getTypeDescriptor = function(path) {
  var dotPos = path.indexOf('.');
  if(dotPos === -1) {
    //just a field name
    if(path.indexOf('_') !== 0) {
      return this.type_desc_map[path];
    }
    return null;
  }

  var localField = path.substring(0, dotPos);
  var subPath = path.substring(dotPos+1);

  var localTd = this.type_desc_map[localField];
  var isArray = localTd instanceof Array; 
  if(isArray){
    localTd = localTd[0];
  }
  if(localTd && localTd.type === 'reference' && modelCache[localTd.ref_class]) {
    var refModel = modelCache[localTd.ref_class];
    return refModel._bo_meta_data.getTypeDescriptor(subPath);
  }
  else if(localTd && localTd.is_composite) {
    //TODO this only works to go one level deep, disallowing composites or refs w/in composites!
    // probably need composite's nested type_desc_map to be augmented w/ getTypeDescriptor function....
    return localTd.type_desc_map && localTd.getTypeDescriptor(subPath);
  }
  else { 
    //dotted into a non-reference or a non-existent field:
    logger.debug('attempted to get TD for class %s path %s', this.class_name, path);
    return null;
  }
}

const augmentCompositeTypeDescriptors = function(tdMap) {
  _.forEach(tdMap, (td, fieldName)=>{
    var isArray = td instanceof Array;
    td = isArray ? td[0] : td;
    if(td.is_composite) {
      td.getTypeDescriptor = getTypeDescriptor.bind(td);
      augmentCompositeTypeDescriptors(td.type_desc_map);
    }
  });
};



const BoMetaData = function(className, bodId, typeDescMap, superClassRef) {
  this.class_name = className;
  this.bod_id = bodId;

  this.type_descriptor = typeDescMap;  //need to clean up old code that uses this badly-named
  this.type_desc_map = typeDescMap;

  if(superClassRef) {
    const SuperModel = modelById[superClassRef._id];
    _.merge(this.type_desc_map, SuperModel._bo_meta_data.type_desc_map);
    this.super_model = SuperModel;
  }

  augmentCompositeTypeDescriptors(typeDescMap);

};

BoMetaData.prototype.getTypeDescriptor = getTypeDescriptor;

/**
 * Creates the _bo_meta_data object attached to every BusinessObject model
 **/
var createMetaObj = function(forBod) {
  var typeDescMap = _.cloneDeep(forBod.definition);
  FieldTypeService.augmentTypeDescMap(typeDescMap);
  return new BoMetaData(forBod.class_name, forBod._id, typeDescMap, forBod.superclass);
};


/**
 * Computes/refreshes child_models metadata for each model
 */
const buildChildClassMetadata = function() {
  const superToChildren = {};
  _.forEach(modelCache, (model, className)=>{
    if(model === BusinessObjectDefBootstrapModel) return;
    
    let md = model[Symbol.for('metadata')];
    delete md.child_models;

    let superBodId = md.super_model && md.super_model[Symbol.for('metadata')].bod_id;
    if(superBodId) {
      let childArr = superToChildren[superBodId] = superToChildren[superBodId] || [];
      childArr.push(model);
    }  
  });
  _.forEach(superToChildren, (childModelArr, superBodId)=>{
    let md = modelById[superBodId][Symbol.for('metadata')];
    md.child_models = childModelArr;
  });
};

/**
 * Converts the schema JSON we use in our BusinessObjectDef.definition into a mongo schema,
 *  with the help of FieldTypeService to convert custom field types to Mongoose types.
 **/
var createMongoSchema = function(forBod) {

  var mongoSchemaDef = {};
  // var indexObj = {};


  //Iterate through fields in BOD's definition,
  //  mapping each to mongoose type specifiers via fieldTypes
  for(var fieldName in forBod.definition) {
    if(fieldName.indexOf('_') === 0)
      continue;

    var td = forBod.definition[fieldName];
    var isArray = false;
    if(Array.isArray(td) ) {
      isArray = true;
      td = td[0];
    }

    var mongoType = FieldTypeService.getSchemaElem(td);

    if(!mongoType) {
      logger.error('Unknown type "%s" in schema for %s.%s', td.type, forBod.class_name, fieldName);
      continue;
    }

    if(isArray)
      mongoType.type = [mongoType.type];

    mongoSchemaDef[fieldName] = mongoType;

    // if(mongoType.textIndex) {
  //     logger.("SETTING TEXT INDEX ON %s . %s", forBod.class_name, fieldName);
  //     indexObj[fieldName] = 'text';
    // }
  }


  //Since we're defining our own id generator that creates a id of type string:
  mongoSchemaDef._id = {
    type: String,
    // index: {
    //     unique: true
    // }
  };

  //for versioning:
  mongoSchemaDef.__ver = Mixed; //ObjectId;

  //__pkg attribute to differentiate arbitrary objects based on package
  mongoSchemaDef.__pkg = String;
  
  //__disp attribute persists dynamically-generated _disp field.
  mongoSchemaDef.__disp = String;
  
  mongoSchemaDef.__match_text = Mixed;

  var mongoSchema;
  var schemaOptions = {collection:forBod.class_name, usePushEach: true};

  if(forBod.abstract) {
    //This BOD represents a superclass to someone;
    // create a special constructor for it...
    var AbstractSchema = function() {
        Schema.apply(this, arguments);
        this.add(mongoSchemaDef); //TODO, don't add overridden fields...
    };
    util.inherits(AbstractSchema, Schema);
    customSchemaConstructors[forBod._id] = AbstractSchema;

    mongoSchema = new AbstractSchema({}, schemaOptions);
  }
  else if(forBod.superclass) {
    var superId = forBod.superclass._id;
    if(!superId || !customSchemaConstructors[superId]) {
      logger.error("Error in BusinessObjectDef, bad superclass ref in %s - %j", forBod.class_name, forBod.superclass);
      return null;
    }
    //This BOD represents a subclass of another;
    // use superclass's special schema constructor
    var SuperSchema = customSchemaConstructors[superId];
    mongoSchema = new SuperSchema(mongoSchemaDef);
  }
  else {
    schemaOptions.versionKey = false;
    mongoSchema = new Schema(mongoSchemaDef, schemaOptions);
  }

  //add a virtual _disp field
  mongoSchema.virtual('_disp').get(dispGenerator);
  mongoSchema.virtual('_match_text').get(matchTextGenerator);


  //Add text indexing
  // if(Object.keys(indexObj).length > 0) {
  //   mongoSchema.plugin(mongooseTextSearch);
  //   mongoSchema.index(indexObj);
  // }

  //Wire in UUID generation
  mongoSchema.pre('save', idGenerator);
  
  //add in noonian middleware
  interceptor.registerHooks(mongoSchema);


  return mongoSchema;
};


/**
 *  Pulls in provided BusinessObject definition and creates a corresponding Mongoose model
 **/
var createAndCacheModel = function(forBod) {
  var className = forBod.class_name;
  logger.debug("Initializing mongoose model for %s", className);

  if(modelCache[className]) {
    logger.warn('Attempting to cache a DB model multiple times: %s', className);
    return;
  }

  var connKey = forBod.system ? 'system' : 'application';
  var mongooseConn = mongooseConnections[connKey] || mongooseConnections.default;
  
  var mongoSchema = createMongoSchema(forBod);
  if(!mongoSchema) {
    logger.warn("Skipping model creation for %s", className);
    return;
  }

  var mongoModel;
  if(forBod.superclass) {
    //It's a subclass of someone else;
    //  create model using "discriminator" factory of super-class's model
    var SuperModel = modelById[forBod.superclass._id];
    
    //Little sketchy here, digging into mongoose internals..
    // (need to do in case we're re-initializing a BOD that has a super class)
    if(SuperModel.discriminators && SuperModel.discriminators[className]) {
      delete SuperModel.discriminators[className];
    }
    //end sketchiness
        
    mongoModel = SuperModel.discriminator(className, mongoSchema);
  }
  else {
    mongoModel = mongooseConn.model(className, mongoSchema);
  }


  const propDef = {
    enumerable:false,
    writable:true,
    value:createMetaObj(forBod)
  };
  
  Object.defineProperty(mongoModel, '_bo_meta_data', propDef);
  Object.defineProperty(mongoModel, Symbol.for('metadata'), propDef);
  Object.defineProperty(mongoModel.prototype, '_bo_meta_data', propDef);
  Object.defineProperty(mongoModel.prototype, Symbol.for('metadata'), propDef);

  //Additional member functions and proxies
  interceptor.decorateModel(mongoModel);

  //Put it in the cache
  modelCache[className] = mongoModel;
  modelById[forBod._id] = modelCache[className];

  exports[className] = modelCache[className];
  exports[forBod._id] = modelCache[className];
  logger.verbose("Initialized mongoose model for %s", className);
  return mongoModel.count({}).then(x=>logger.debug('COUNT: %s', x));
};



var augmentModelsWithMemberFunctions = function(singleClass) {
  if(!modelCache.MemberFunction) {
    return;
  }
  
  var queryObj = {};
  if(singleClass && modelCache[singleClass]) {
      queryObj.business_object = modelCache[singleClass]._bo_meta_data.bod_id;
  }

    
  return modelCache.MemberFunction.find(queryObj).then(function(memberFnList) {
    _.forEach(memberFnList, function(memberFnObj) {
      
      if(memberFnObj.business_object &&
         memberFnObj.name &&
         memberFnObj.function &&
         (memberFnObj.applies_to == 'server' || memberFnObj.applies_to == 'both')) {
        
        logger.verbose('Installing Member Function %s.%s', memberFnObj.business_object._disp, memberFnObj.name);
        
        var ModelObj = modelById[memberFnObj.business_object._id];

        if(!ModelObj) {
          logger.error('Invalid BOD ref in MemberFunction object')
          return;
        }
        
        var targetObj;
        if(memberFnObj.is_static) {
          targetObj = ModelObj;
        }
        else {
          targetObj = ModelObj.prototype;
        }

        var theFunction;
        if(memberFnObj.use_injection) {
          try {
            theFunction = invokerTool.invokeInjected(memberFnObj.function, {}, memberFnObj);
          }
          catch(e) {
            logger.error(e);
          }
          if(!theFunction) {
            logger.error('Invalid return value for MemberFunction %s.%s', memberFnObj.business_object, memberFnObj.name);
            return ;
          }
        }
        else {
          theFunction = memberFnObj.function;
        }

        if(theFunction) {
          Object.defineProperty(targetObj, memberFnObj.name, {
            enumerable:false,
            writable:true,
            value:theFunction
          });
        }

      }

    });
  });
};

/**
 * Initializes cache of Mongoose Models from BusinessObjectDef's in the system...
 **/
var buildModelCache = function() {

  return BusinessObjectDefBootstrapModel.find({}).exec().then(function(objList) {

    //First, arrange our BusinessObjectDefs so superclasses come first
    objList.sort(function(x,y) {
      if(!y.abstract === !x.abstract) return 0;
      else if(y.abstract && !x.abstract) return 1;
      else return -1;
    });

    //Next, create the models one-by-one
    for(var i=0; i < objList.length; i++) {
      createAndCacheModel(objList[i]);
    }

    buildChildClassMetadata();
  });
};




/**
 * - called w/ the BOD instance as "this"
 * - calls BOD save(), thereafter updating our Model cache to add/update the new BOD
 * @param versionId - __ver to use when updating BusinessObjectDef record (used when a package is being installed to keep consistent w/ package manifest)
 **/
var deferredBodSave = function(versionId) {
  versionId = versionId || this.__ver;
  return this.save({useVersionId:versionId, skipTriggers:true}, null).then(
    function(obj){
      logger.debug("Successfully saved BusinessObjectDef: %s", obj.class_name);
      createAndCacheModel(obj);
    },
    function(err) {
      logger.error("ERROR saving BusinessObjectDef ").error(err);
    }
  );
}

/**
 *  Wipe from existence the model w/ specified className
 **/
var clearModel = function(className) {
  delete modelCache[className];
  //little dangerous - digging into mongoose internals:
  _.forEach(mongooseConnections, mongooseConn=>{
    delete mongooseConn.models[className];
  });
};


var pendingBodsToInstall = {};
/**
 * Creates or updates BOD in the database, and adds it to the Model cache
 * @param bodObj - a plain-object representation of a BusinessObjectDef
 **/
var installBusinessObjectDef =
exports.installBusinessObjectDef = function(bodObj) {
  var className = bodObj.class_name;
  logger.debug('Installing BOD model for %s', className);

  if(bodObj.superclass && !customSchemaConstructors[bodObj.superclass._id]) {
    //Super class hasn't been loaded yet... add it to pending list to defer it's loading
    logger.debug('...deferring BOD install for %s', className);
    var superId = bodObj.superclass._id;
    pendingBodsToInstall[superId] = pendingBodsToInstall[superId] || [];
    pendingBodsToInstall[superId].push(bodObj);
    return Q(true);
  }

  var BusinessObjectDef = modelCache.BusinessObjectDef; //In case we're bootstrapping BusinessObjectDef!

  if(modelCache[className]) {
    clearModel(className);
  }

  return BusinessObjectDef.findOne({_id:bodObj._id}).then(function(currBod) {
    if(currBod) {
      var keepVersion;
      if(bodObj.__ver) {
        //We want to retain the __ver that was passed in so as to keep consistent w/ package manifest.
        keepVersion = bodObj.__ver;
        delete bodObj.__ver;
      }
      _.assign(currBod, bodObj); //write onto currBod any fields from passed-in bodObj
      return deferredBodSave.apply(currBod, [keepVersion]);
    }
    else {
      //Not in the DB; do an insert
      return deferredBodSave.apply(new BusinessObjectDef(bodObj));
    }

  })
  .then(function() {
      //restore any memberfunctions we may have trampled
      return augmentModelsWithMemberFunctions(className);
  })
  .then(function() {
      //Any BODs waiting for this one
      if(pendingBodsToInstall[bodObj._id]) {
        var promiseChain = Q(true);
        _.forEach(pendingBodsToInstall[bodObj._id], function(pendingBod) {
          promiseChain = promiseChain.then(installBusinessObjectDef.bind(null, pendingBod));
        });
        promiseChain = promiseChain.then(function() {
          delete pendingBodsToInstall[bodObj._id];
        });
        return promiseChain;
      }
  })
  .then(buildChildClassMetadata)
  ;

};




/**
 * When a BusinessObjectDef changes, sync our model cache to reflect the changes
 * @this - the BusinessObjectDef object
 * @param isCreate, isDelete - injected from DataTrigger logic
 **/
var bodUpdate = function(isCreate, isDelete) {
  var className = this.class_name || this._previous.class_name;
  logger.verbose('Refreshing model cache on %s', className);

  if(!isCreate) {
    clearModel(className);
  }

  if(!isDelete) {
    createAndCacheModel(this);
  }

  //In case there are any updates to the parent/child class structure
  buildChildClassMetadata();
};

const memberFnUpdate = function(isCreate, isDelete) {
  var bo = this.business_object && this.business_object._id || 
    this._previous && this._previous.business_object && this._previous.business_object._id;
  
  if(!bo || !modelById[bo]) return;

  var className = modelById[bo][METADATA].class_name;

  logger.verbose('Refreshing model cache on %s', className);
  return augmentModelsWithMemberFunctions(className);
};


const connectMongoose = function() {

  mongoose.Promise = Q.Promise;
  
  var mongoConf = serverConf.mongo;

  if(!mongoConf || !Object.keys(mongoConf).length) {
    throw new Error('Invalid mongo configuration in instance-config.js')
  }

  // https://mongoosejs.com/docs/deprecations.html
  mongoose.set('useNewUrlParser', true);
  mongoose.set('useFindAndModify', false);
  mongoose.set('useCreateIndex', true);
  mongoose.set('useUnifiedTopology', true);

  //Basic case: single mongo connection
  if(mongoConf.uri) {
    console.log('Loading non-segmented db configuration...', mongoConf);
    mongoConf = serverConf.mongo = {
      default:mongoConf
    };
  }

  const connPromises = [];

  Object.keys(mongoConf).forEach(connKey=>{
    let myConf = mongoConf[connKey];

    if(myConf) {
      let myUri = myConf.uri;
      let myOpts = myConf.options || {};

      logger.verbose('attempting mongoose connect: %s | %j', myUri, myOpts);

      let myConn = mongooseConnections[connKey] = mongoose.createConnection(myUri, myOpts);
      connPromises.push(myConn);
    }

  });

  return Q.all(connPromises).then(()=>mongooseConnections, err=>{
    console.error('Error connecting to mongo');
    console.error(err);
  });
};


/**
 * Bootstrap a clean database
 **/
exports.bootstrapDatabase = function(adminPw) {

  // mongoose.Promise = Q.Promise;
    // Connect to database
  // const mongoOpts = serverConf.mongo.options = serverConf.mongo.options || {};

  // logger.verbose('attempting mongoose connect', serverConf.mongo.uri, mongoOpts);
  // return mongoose.connect(serverConf.mongo.uri, mongoOpts).then(()=>{
  return connectMongoose().then(()=>{
    getBootstrapModel();

    // no FieldType BOD in the system!  Therefore it's a fresh DB...
    logger.info('BOOTSTRAPPING DATABASE: %s', serverConf.mongo.uri);

    //Set up to use Bootstrap BusinessObjectDef; will get updated w/ proper model when sys is loaded
    modelCache.BusinessObjectDef = exports.BusinessObjectDef = BusinessObjectDefBootstrapModel;


    return PackagingService.installBootstrapPackages()
      .then(FieldTypeService.init)
      .then(function(){
        //Refresh models now that FieldTypeService has been properly initialized
        // (all Models were created sans FieldType objects)
        logger.info('Refreshing BOD models');
        _.forEach(modelCache, function(model, className) {
          clearModel(className);
        });
      })
      .then(buildModelCache,
        function(err) {
          logger.error('Error bootstrap').error(err);
          process.exit();
        }
      )
      .then(RefService.repair)
      .then(function() { //Set up admin's password
        return modelCache.User.findOne({name:'admin'}).exec();
      })
      .then(function(adminUser) {
        var newPw = adminPw || generateId();

        adminUser = adminUser || new modelCache.User({
          name:'admin',
          roles:[{
            _disp:'ROLE_SYSADMIN',
            _id:'FnQ_eBYITOSC8kJA4Zul5g'
          }]
        });

        adminUser.password = newPw;
        
        return adminUser.save().then(function() {
          if(adminPw) {
            console.error('User "admin" created with specified password');
          }
          else {
            console.error('***** ADMIN PASSWORD GENERATED ******');
            console.error('User "admin" created with password:');
            console.error(newPw);
            console.error('*************************************');
          }
        });

      })
      .then(function() {
        //initialize config for instance name:
        return modelCache.Config.findOne({key:'sys.instanceName'}).exec();
      })
      .then(function(inCfg) {
        inCfg = inCfg || new modelCache.Config({
          key:'sys.instanceName',
        });
        inCfg.value = serverConf.instanceName;
        return inCfg.save();
      });
      ;
  });
};

/**
 * Completely refresh data layer (called on package install)
 */
exports.refreshModels = function() {
  logger.info('Refreshing data layer');
  Object.keys(modelCache).forEach(clearModel);
  return buildModelCache()
    .then(()=>augmentModelsWithMemberFunctions());
};

/**
 * Initialize mongo connection and data layer.
 *  If no BusinessObjects are found in the database, a bootstrap is performed.
 * @return promise that is fulfilled upon completion.
 **/
exports.init = function(conf) {
  
  // mongoose.Promise = Q.Promise;

  // Connect to database
  // const mongoOpts = conf.mongo.options = conf.mongo.options || {};

  // logger.verbose('attempting mongoose connect', serverConf.mongo.uri, mongoOpts);
  // return mongoose.connect(serverConf.mongo.uri, mongoOpts).then(()=>{
  return connectMongoose().then((conns)=>{
    logger.debug('CONNECTED: %j', Object.keys(conns));

    //perform data layer bootstrap by first looking for the FieldType BOD via our minimal BusinessObjectDef bootstrap model
    return getBootstrapModel().findOne({class_name:'FieldType'})
    .then(function(fieldTypeBod) {
      //Load FieldType BOD description -> schema/model cache
      if(fieldTypeBod) {
        logger.verbose('Initializing data layer from DB: %j', conf.mongo);

        //Make it so db.FieldType is available
        createAndCacheModel(fieldTypeBod);

        //Now FieldTypeService.init() can pull in all the FieldType objects,
        //  so buildModelCache can properly build schemas/models for all of the BOD definitions.
        return FieldTypeService.init().then(clearModel.bind(null, 'FieldType')).then(buildModelCache);

      }
      else {
        throw new Error(`Invalid noonian database "${conf.mongo.uri}".  Did you remember to bootstrap?`)
      }

    })
    .then(function() { //system is bootstrapped... finish initializing the rest of the data layer
      return DataTriggerService.init()
        .then(QueryOpService.init)
        .then(GridFsService.init.bind(null, conf))
        .then(RefService.init.bind(null, conf))
        .then(PackagingService.init)
        .then(invokerTool.init)
        .then(augmentModelsWithMemberFunctions)
        .then(function() {
          //Register a data trigger so that future BOD updates
          DataTriggerService.registerDataTrigger('sys.internal.dbUpdate', 'R1r6pCVESdma9hj8GrfMaQ', 'after', true, true, true, bodUpdate,-1);

          //MemberFunction Updates
          DataTriggerService.registerDataTrigger('sys.internal.memberFnUpdate', 'hlNq-6YtR_mr_AFSaLfcAw', 'after', true, true, true, memberFnUpdate,-1);

          //Data trigger for "created_date", "modified_date",
          DataTriggerService.registerDataTrigger('sys.internal.datestamps', null, 'before', true, true, false,
            function(isCreate) {
              if(isCreate && this._bo_meta_data.type_desc_map.created_date) {
                this.created_date = new Date();
              }
              if(this._bo_meta_data.type_desc_map.modified_date) {
                this.modified_date = new Date();
              }
              //_current_user
              if(this._current_user) {
                  if(isCreate && this._bo_meta_data.type_desc_map.created_by) {
                      this.created_by = {_id:this._current_user._id, _disp:this._current_user._disp};
                  }
                  if(this._bo_meta_data.type_desc_map.modified_by) {
                      this.modified_by = {_id:this._current_user._id, _disp:this._current_user._disp};
                  }
              }
            }
          );
          
          //Data trigger for "created_date", "modified_date",
          DataTriggerService.registerDataTrigger('sys.internal.disp', null, 'before', true, true, false,
            function() {
                if(this._bo_meta_data.type_desc_map._disp) {
                  this.__disp = this._disp;
                }
                if(this._bo_meta_data.type_desc_map._fulltext_matches) {
                  this.__match_text = this._match_text;
                }
            }
          );


        });
    });
  });
}


