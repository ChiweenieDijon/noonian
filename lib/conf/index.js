'use strict';

const path = require('path');
const _ = require('lodash');



// All configurations will extend these options
// ============================================
const baseConfig = {

  // Root path of server
  root: path.normalize(__dirname + '/../..'),

  // Server listen port/host - defaults to 9000 (overwritten by instance conf)
  serverListen: {
    port: 9000,
    host: '127.0.0.1'
  },

  useHttps:false,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'noonian-secret'
  },

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/noonian-dev'
  },


  instanceName:'Unspecified',
  instanceId:'0',
  urlBase:'',

  logLevel:'debug',
  // logging:{
  //   sys:{
  //     file:'instance.log',  //alternatively, use files:{level:'filename.log', }
  //     colors:true,
  //     level:'debug'
  //   }
  // },

  bootstrap: require('./bootstrap')

};


exports.init = function(commandline) {
  const instanceDir = process.env.NOONIAN_INSTANCE;
  var instanceConf;

  if(!instanceDir) {
    console.error('Missing NOONIAN_INSTANCE envirionment variable.  Try launching via Noonian CLI');
    process.exit(1);
  }

  try {
    instanceConf = require(path.resolve(instanceDir, 'instance-config.js'));
  }
  catch(err) {
    console.error('Problem loading instance configuration at %s', instanceDir).error(err);
    process.exit(1);
  }

  const fullConfig = _.assign(baseConfig, instanceConf);

  fullConfig.instanceDir = instanceDir;

  
  //make sure urlBase has a leading slash if it isn't empty
  if(fullConfig.urlBase === '/') {
    fullConfig.urlBase = '';
  }
  else if(fullConfig.urlBase && fullConfig.urlBase.indexOf('/') !== 0) {
    //in config file, urlBase may or may not have leading slash
    fullConfig.urlBase = '/'+fullConfig.urlBase;
  }

  fullConfig.packageJson = require('../../package.json');

  _.assign(exports, fullConfig);
};

exports.displayInfo = function(logger) {
  logger = logger || console;
  logger.info('Noonian Version %s', exports.packageJson.version);
  logger.info('Instance Configuration for "%s"', exports.instanceName);
  logger.info('  Directory \t %s', exports.instanceDir);
  logger.info('  URL Base \t %s', exports.urlBase || '/');
  logger.info('  Listen on \t %s:%s', exports.serverListen.host, exports.serverListen.port);
  logger.info('  MongoDB \t %s', exports.mongo.uri||JSON.stringify(exports.mongo, null, 2));
};

