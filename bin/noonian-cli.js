#!/usr/bin/env node

const commander = require('commander');
const colors = require('colors'); //Augments string prototype to add colors to console output

const pkgJson = require('../package.json');


const start = function() {
	require('../lib/cli/startstop').start.apply(null, arguments);
};
const stop = function() {
	require('../lib/cli/startstop').stop.apply(null, arguments);
};
const getEnv = function() {
	require('../lib/cli/startstop').getEnv.apply(null, arguments);
};
const restart = function() {
	require('../lib/cli/startstop').restart.apply(null, arguments);
};
const pm2Eco = function() {
	require('../lib/cli/startstop').pm2Ecosystem.apply(null, arguments);
};
const startall = function() {
	require('../lib/cli/startstop').startall.apply(null, arguments);
};


const list = function() {
	require('../lib/cli/liststatus').list.apply(null, arguments);
};
const status = function() {
	require('../lib/cli/liststatus').status.apply(null, arguments);
};


const init = function() {
	require('../lib/cli/initbootstrap').init.apply(null, arguments);
};
const bootstrap = function() {
	require('../lib/cli/initbootstrap').bootstrap.apply(null, arguments);
};


const add = function() {
	require('../lib/cli/addremove').add.apply(null, arguments);
};
const remove = function() {
	require('../lib/cli/addremove').remove.apply(null, arguments);
};


const dumpDb = function() {
	require('../lib/cli/mongo').dumpDb.apply(null, arguments);
};
const dbShell = function() {
	require('../lib/cli/mongo').dbShell.apply(null, arguments);
};


const launchBrowser = function() {
	require('../lib/cli/util').launchBrowser.apply(null, arguments);
};
const watchConsole = function() {
	require('../lib/cli/util').watchConsole.apply(null, arguments);
};



commander.version(pkgJson.version);

commander
	.command('start [instanceName]')
	.description('launch an instance')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.option('-w --watch', 'Watch stdout/stderr logs after instance is started')
	.option('-o --open', 'open browser window after instance is started')
	.action(start);

commander
	.command('stop [instanceName]')
	.description('stop a running instance')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.action(stop);

commander
	.command('restart [instanceName]')
	.description('restart a running instance')
	.option('-d --directory <instanceDir>', 'Instance Directory')	
	.option('-w --watch', 'Watch stdout/stderr logs after instance is started')
	.action(restart);

commander
	.command('env [instanceName]')
	.description('show environment variables for launching an instance (returns source-able list of variable export lines)')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.action(getEnv);

commander
	.command('list')
	.description('list configured instances')
	.action(list);

commander
	.command('status')
	.description('list the currently-running Noonian instances')
	.action(status);

commander
	.command('startall')
	.description('Start all of the configured Noonian instances')
	.action(startall);


commander
	.command('init <instanceName>')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.option('--db <mongoDbName>', 'Name of MongoDB Database')
	.option('--dbAddress <address>', 'IP address of database')
	.option('--dbUri <uri>', 'Full MongoDB connection URI (overrides --db and --dbAddress options)')
	.option('--listenPort <number>', 'Port number on which to listen')
	.option('--listenAddress <address>', 'Address on which to listen (default 127.0.0.1')
	.option('--instanceId <instanceId>', 'ID string for instance')
	.description('initialize directory for a instance')
	.action(init);

commander
	.command('bootstrap [instanceName]')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.option('-p --password [adminPassword]', 'Set initial password for "admin" user account (prompt for one if none provided)')
	.option('-v --verbose', 'Verbose output')
	.description('bootstrap database for an instance')
	.action(bootstrap);


commander
	.command('add')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.description('add an existing instance to the index')
	.action(add);

commander
	.command('remove <instanceName>')
	.description('remove an instance from the instance index')
	.action(remove);


commander
	.command('dbdump [instanceName]')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.option('-z --zip', 'compress the dump to a single archive')
	.description('call mongodump to create a dump of the database')
	.action(dumpDb);

commander
	.command('dbshell [instanceName]')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.description('start mongo shell for the instance\'s database')
	.action(dbShell);


commander
	.command('open [instanceName]')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.description('launch browser window')
	.action(launchBrowser);

commander
	.command('watch [instanceName]')
	.option('-d --directory <instanceDir>', 'Instance Directory')
	.description('watch stdout and stderr of an instance')
	.action(watchConsole);


commander
	.command('pm2-eco')
	.description('generate pm2 ecosystem JSON (print to stdout)')
	.action(pm2Eco);
//TODO install-pkg, reset-pw, console



commander.parse(process.argv);

// if(!commander.args.length) {
// 	commander.outputHelp();
// }
