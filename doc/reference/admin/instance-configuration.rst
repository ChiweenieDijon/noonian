Instance Configuration
======================

Configuration for a Noonian instance is stored in a javascript file in
the base of the instance directory: **instance-config.js**. A default is
generated from a template when an instance is initialized using the CLI.


Configuration File
------------------



Required Parameters
~~~~~~~~~~~~~~~~~~~

At instance initialization, all required parameters are
populated/generated with reasonable defaults.

**instanceName**

A name used for referring to the instance via the CLI. Should be unique
across instances on a server for the CLI to work properly.

**instanceId**

An identifier used in versioning the persisted business objects. It
should be short; by default it matches the instance name.

**serverListen**

TCP Port and address on which to bind. On initialization, the Noonian
CLI examines the config files for all configured instances on the
machine and chooses a port number by adding 1 to the highest one found.

.. code:: js

      serverListen: {
        port: 11100,
        host: '127.0.0.1'
      }

**mongo**

MongoDB connection string and options. Initialization defaults to
localhost

.. code:: js

      mongo: {
        uri: 'mongodb://localhost/noonian-myinstance'
      }

**secrets**

Secret string used for signing the auth tokens for authentication.
Initialization generates a random UUID.

.. code:: js

      secrets: {
        session: '1f6885e7-36a8-4dd9-909e-d585e5bd0879'
      }

Logging
~~~~~~~

By default, log level is set to debug, writing logs to instance.log (all
log messages) and error.log (only error messages).

Logging can be configured via either of the following parameters.

Alternatively, logging can be configured via the logging-config.js in
the instance directory.

**logLevel**

For simple configuration if the default files/formats are sufficient.
Set it to one of:

*error*, *warn*, *info*, *verbose*, *debug*, *silly*

Logs messages at or above the configured level are written to
instance.log, and errors to error.log.

**loggers**

Provides more detailed control over log formats and transports.


Optional Parameters
~~~~~~~~~~~~~~~~~~~

**urlBase** (string)

A path under which the noonian instance will be served.

Use urlBase if noonian will be served to a path other than the root of
the domain. For example, one domain can host multiple noonian instances
as such: mydomain.com/instance1, mydomain.com/instance2

**enableBackRefs**

Enable "back reference" processing on reference fields.

**enableHistory**



Instance Directory Structure
----------------------------

The files and directories in the instance directory are described below.

*  **instance-config.js**
*  **client** - base directory for static content served from the
   filesystem by the webserver (this directory is checked BEFORE client
   directory of the core app listed above)
   *  **bower\_components** - holds bower dependencies of packages
   installed on this instance
*  **node\_modules** - holds any npm dependencies of packages installed
   on this instance
*  *[pkg-key]* - if package building with filesystem sync is enabled,
   data updates are synced to this directory
