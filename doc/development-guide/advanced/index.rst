Advanced Customization
======================

This section is describes how to extend the fundamental objects of Noonian and the DBUI.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   field-types
   query-ops
   

