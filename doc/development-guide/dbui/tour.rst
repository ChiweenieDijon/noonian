A Tour of the DBUI
==================


When you first log in to a fresh instance, you are greeted with the default home screen:

.. figure:: home-screen-full.png
   :align: center
   :alt: Noonian DBUI Home Screen


At the top we've got the **navbar**, on the left we've got the **sidebar**, and the majority of the screen is our main content area.


The Navbar
----------

.. figure:: navbar.png
   :align: center
   :alt: Navbar

The top bar we'll refer to as the **navbar** (this is by convention; in actuality most navigation in a Noonian system occurs via the sidebar)

Starting from the left, the navbar components are:

Page Title
~~~~~~~~~~

   Displays the configured instance name.  When clicked, takes user to the configurable "home" screen, which is the same screen displayed when the user first logs in.


System Menu
~~~~~~~~~~~

.. figure:: navbar-system-menu.png
   :align: center
   :alt: System Menu


To the right of the title, the configured dropdown menus are displayed.  Menus can be customized based on user role.  

The **System** menu is the default navbar menu for the system admin role, and shows logical groupings of all :term:`Business Object` classes in the system.


Notifications
~~~~~~~~~~~~~

The bell icon on the right-hand side of the navbar contains a list of alert messages that the system has displayed to the user during their session.


User Menu
~~~~~~~~~

.. figure:: user-menu.png
   :align: center
   :alt: User Menu

The rightmost icon on the navbar is for user profile management (password reset, etc.) and log-out.  



The Sidebar
-----------

.. figure:: sidebar.png
   :align: center
   :alt: Sidebar

The sidebar is the primary mechanism by which the user navigates a Noonian system.  The content of the sidebar is customizable based on user role.

The default sidebar for the system admin role is the *Developer Menu*, providing access to the components used for developing a Noonian application. 


Gear Menu
~~~~~~~~~

.. figure:: sidebar-gear-menu.png
   :align: center
   :alt: Sidebar Gear Menu

The gear icon on the upper left-hand side of the sidebar allows the user to select which sidebar menu is displayed.

Additionally, it provides an option to collapse all submenus on the sidebar.


Minimize and Expand
~~~~~~~~~~~~~~~~~~~

.. figure:: sidebar-minimize.png
   :align: center

The double chevron icon on the top right of the sidebar provides a way to reduce the size of the sidebar.  Clicking it reduces the display to show only icons for each submenu title.  In minimized mode, a submenu is exposed when the mouse hovers over its submenu icon.




Home Screen
-----------

.. figure:: home-screen.png
   :align: center
   :alt: Home Screen

When the user initially logs in, the main content area is populated with a configurable home screen.  This is the same screen that is shown when the user clicks the title on top left of the navbar.  

The default home screen on a fresh instance provides information on how to customize it.



List, Edit, and View Business Objects
-------------------------------------

The core functionality of the Noonan DBUI is in editing, viewing and querying :term:`Business Objects <Business Object>` in the database.  There are 3 primary screens for doing so: **list**, **edit**, and **view**.


List
~~~~

The **list** screen displays a list of Business Objects in the system.  It includes search and query tools and configurable action buttons.

It happens that all of the links on the Developer Menu actually link to a **list** screen.  As an example, let's take a look at the configuration list, since there are a number of objects to be seen on a fresh instance:

.. figure:: sidebar-config.png
   :align: center
   :alt: Sidebar -> Admin -> Configuration


.. figure:: config-list.png
   :align: center
   :alt: Config List


You see a filtered list of **Config** Business Objects in the system, and several buttons and tools.


.. figure:: config-list-annotated.png
   :align: center
   :alt: Config List With Labels


Action Bar
   Shows buttons for configured actions for the current :term:`perspective`.

Search/Query Tool
   Allows for a quick text search, or opening the Query Builder to perform more sophisticated query on the data.

Gear Menu
   Shows a dropdown to perform table-related actions, such as editing the displayed columns, or refreshing or exporting the data. 

:term:`Record Actions <RecordAction>`
   Shows record-specific actions that can be invoked for a specific row.  The :fa:`eye` **View**  and :fa:`edit` **Edit** actions are generally present for the default list :term:`perspectives <perspective>`.



Edit
~~~~

The **edit** screen displays a form for editing a single Business Object.  It includes layout editing tools and configurable action buttons.


As an example, let's take a look at an **edit** screen for a User Business Object:

.. figure:: sidebar-user-edit.png
   :align: center
   :alt: Sidebar -> Admin -> Users


.. figure:: user-edit-annotated.png
   :align: center
   :alt: User Edit

Action Bar
   Shows buttons for configured actions for the current :term:`perspective`.

Layout Editor
   Shows a dialog that allows the user to change the fields displayed and the layout of the form.

:term:`Record Actions <RecordAction>`
   Shows record-specific actions that can be invoked for the object being edited.  The :fa:`save` **Save**, :fa:`times-circle` **Delete**, :fa:`eye` **View**, and :fa:`download` **Export** actions are generally present for the default edit :term:`perspectives <perspective>`.



View
~~~~

The **view** screen is very similar to an edit screen, the obvious difference being the editability of the displayed Business Object.

As an example, let's look at the corresponding **view** for the User object we examined above.  Use the :fa:`eye` **View** button from the edit screen:

.. figure:: user-edit-to-view.png
   :align: center
   :alt: view user


.. figure:: user-view-annotated.png
   :align: center
   :alt: User Edit

Action Bar
   Shows buttons for configured actions for the current :term:`perspective`.

Layout Editor
   Shows a dialog that allows the user to change the fields displayed and the layout of the form.

:term:`Record Actions <RecordAction>`
   Shows record-specific actions that can be invoked for the object being edited.  The :fa:`edit` **Edit**, :fa:`times-circle` **Delete**, :fa:`copy` **Duplicate**, and :fa:`download` **Export** actions are generally present for the default view :term:`perspectives <perspective>`.

