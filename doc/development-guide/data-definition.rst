Data Definition
===============



The Business Object
-------------------

Business Objects are the basic building blocks of any Noonian application.  Defining a Business Object is akin to defining a database table in the SQL world, and resembles class definition in the Object Oriented Programming world.  

In Noonian, you define the the data schema for your application by
creating a set of **Business Object Definitions**.

When you create a Business Object Definition, you're defining structure
for a particular class of Business Object that you want to persist.

That class of Business Object you defined translates to several components 
in a Noonian system:

1. A corresponding collection in MongoDB that contains the persisted objects
2. An API within the server-side Node.js environment for querying, reading, and
   updating the objects
3. An API provided by an Angular.js service that allows you to query, read and
   update from the client-side.
4. A set of screens for viewing, querying, and updating the objects
   within the Angular.js front-end, called the DBUI.


Creating a Business Object Definition
-------------------------------------

Expand the sidebar menu **Data Definition / Serverside Dev**; and select
**BusinessObjectDefs**

.. figure:: DataDefinition1.png
   :alt: BusinessObjectDefs Menu

   BusinessObjectDefs Menu

You are shown the list of BusinessObjectDef's in the system. Select the
**New** button at the top to create a new one.

.. figure:: DataDefinition2.png
   :alt: BusinessObjectDef List -> New

   BusinessObjectDef List -> New


You are shown a form that allows you to specify the fields for your new
Business Object Definition. Let's demonstrate with a basic Person class:

.. figure:: DataDefinition3.png
   :alt: New BusinessObjectDef

   New BusinessObjectDef

The definition is simply a JSON object mapping each field name to its
respective **Type Descriptor** :

.. code:: json

    {
        "name":"string",
        "birthday":"date",
        "phone_number":"phone",
        "address":"physical_address",
        "profile_picture":"image",
        "wears_glasses":"boolean",
        "number_of_children":"integer",
        "misc_notes":"text",
        "user_account":{"type":"reference","ref_class":"User"}
    }

Notice in the example, most of the types are described simple strings:
"string", "date", "boolean", etc. This is a shorthand for those types
that require no more than a single string. The user\_account field is
the exception: its type is "reference", so we need to specify what
class of objects it should reference.

Also perhaps you've noticed how the fields "name" and "misc_notes", have the types "string" and "text", and you're wondering the purpose of the distinction - isn't a "text" field is just a long string?  Indeed it is!  However, by calling it a "text" field, we are 1) creating a more precise description, 2) telling the display logic to use the larger text block for editing it, and 3) telling the system to index it differently.


Let's go ahead and save the BusinessObjectDef.  Notice how those types get
expanded after you save:

.. code:: json

    {
      "name": {
        "type": "string"
      },
      "birthday": {
        "type": "date"
      },
      "phone_number": {
        "type": "phone"
      },
      "address": {
        "type": "physical_address"
      },
      "profile_picture": {
        "type": "image"
      },
      "wears_glasses": {
        "type": "boolean"
      },
      "number_of_children": {
        "type": "integer"
      },
      "misc_notes": {
        "type": "text"
      },
      "user_account": {
        "type": "reference",
        "ref_class": "User"
      }
    }

When you save the a BusinessObjectDef, any single-string values get replaced by a full **Type Descriptor**.  The **Type Descriptor** fully describes a field's type, and at its simplest consists of a single "type" property.  Other properties on a Type Descriptor add neccessary detail in describing the fields type. Some are mandatory (e.g. "ref\_class" for a reference field) , and others are optional (e.g. you may specify a minimum or maximum value of an integer field with a min or max property.)


Please see the `reference page for Field Types <Reference/Server-side/Field-Types>`__ for a complete list of available field types and their respective type descriptor properties.


Indexing
--------

*coming soon*

.. todo:: 
   pending implementation of BusinessObjectDef index specification
