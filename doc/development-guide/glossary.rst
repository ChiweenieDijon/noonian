Glossary
========


Noonian Terminology
-------------------

.. glossary::

   Business Object
      A persisted data object.  

   DBUI
      The graphical user interface of Noonian: "DataBase User Interface"

   Perspective
      A configuration object that describes how to render a DBUI screen: layout, table columns, action buttons, etc.
   
   RecordAction
      An action within the DBUI that is tied to a Business Object when invoked.



Related Jargon
--------------


.. glossary::

   MEAN
      A stack of technologies for web development that consists of Javascript and JSON for both front and back-end:
      M = MongoDB 
      E = Express
      A = AngularJS
      N = Node.js