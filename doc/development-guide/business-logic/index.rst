Adding Business Logic
=====================

This section is will walk you through the structures available to you when developing business logic in your Noonian application.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   data-triggers
   web-services
   code-modules
   member-functions
   schedule-triggers
